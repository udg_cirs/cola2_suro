#!/usr/bin/env python

# Basic ROS imports
import rospy
import tf
import math

# import msgs
from std_msgs.msg import Float32
from geometry_msgs.msg import PoseStamped

class GPSToVelocity:

    def __init__(self):
        self.pub_velocity = rospy.Publisher('/suro/velocity', Float32, queue_size=2)
        self.last_x = 0
        self.last_y = 0
        self.last_time = 0

        # Create Subscriber
        rospy.Subscriber('/suro/navigator/gps_ned',
                         PoseStamped,
                         self.update_gps,
                         queue_size=1)

    def update_gps(self, msg):
        inc_x = msg.pose.position.x - self.last_x
        inc_y = msg.pose.position.y - self.last_y
        m = math.sqrt(inc_x**2 + inc_y**2)
        velocity = Float32()
        velocity.data = m / (msg.header.stamp.to_sec() - self.last_time)
        self.pub_velocity.publish(velocity)
        print 'velocity: ', velocity.data
        self.last_x = msg.pose.position.x
        self.last_y = msg.pose.position.y
        self.last_time = msg.header.stamp.to_sec()

if __name__ == '__main__':
    try:
        rospy.init_node('gps_to_velocity')
        sim_nav_sensors = GPSToVelocity()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
