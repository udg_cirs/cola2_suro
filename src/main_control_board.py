#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.


"""@@This node is used to manage the main control board.@@"""


# ROS imports
import rospy
import serial
import threading
from cola2_lib.rosutils import param_loader
from cola2_lib.rosutils.diagnostic_helper import DiagnosticHelper
from diagnostic_msgs.msg import DiagnosticStatus
from cola2_msgs.srv import DigitalOutput, DigitalOutputResponse


# MAIN CONTROL BOARD COMMANDS
# For firmware V1.2
# V   --> version number
# W   --> 0 or 1, indicating water in the housing
# D0  --> Enable (D01) or disable (D00) digital output 0 for payload (Battery voltage)
# D1  --> Enable (D11) or disable (D10) digital output 1 for payload (24V2 voltage)
# T   --> Temperature of the housing
# R   --> Reset water flag

class MainControlBoard(object):
    """ This node is used to manage the main control board.
     Interrogates periodically the board for checking water ingress and temperature of the housing.
     Provides services to enable/disable payload digital outputs. """

    def __init__(self, name):
        """ Init the class """
        self.name = name

        # Get config from param server
        self.get_config()

        # Mutex handle
        self.lock = threading.Lock()

        # Open serial port
        try:
            self.serial_port = serial.Serial(self.serial_path,
                                             self.serial_baud_rate,
                                             timeout=1)
        except serial.serialutil.SerialException:
            rospy.logerr("Could not open serial port %s", self.serial_path)
            rospy.signal_shutdown("Could not open serial port")


        # Set up diagnostics
        self.diagnostic = DiagnosticHelper(self.name, "hw")

        # Create timer to check water inside
        rospy.Timer(rospy.Duration(2.0), self.check_water)
        rospy.Timer(rospy.Duration(2.0), self.check_temperature)

        # Define services
        self.digital_output_service = rospy.Service('~digital_output',
                                                    DigitalOutput,
                                                    self.digitalOutputSrv)

        # Show initialization message
        rospy.loginfo('initialized')


    def check_water(self, event):
        """ Check if there is water ingress.
            Checks for water ingress inside the housing where the board is installed
            and also for water ingress status that signals water in another external housing. """
        if self.serial_port.isOpen():
            self.lock.acquire()
            # Write request and read answer
            try:
                self.serial_port.write('W\r')
            except:
                rospy.logerr('error sending internal water status request')
            try:
                water_inside = self.serial_port.readline()
            except:
                rospy.logerr('error reading internal water request answer')

            self.lock.release()

            # Check answers
            if 'W=0' in water_inside:
                self.diagnostic.add("water_internal_inside", "False")
                self.diagnostic.set_level(DiagnosticStatus.OK, 'No water')
            elif 'W=1' in water_inside:
                for x in range(20):
                    rospy.logfatal('WATER INSIDE! WATER INSIDE! WATER INSIDE!')
                self.diagnostic.add("water_internal_inside", "True")
                self.diagnostic.set_level(DiagnosticStatus.ERROR, 'Water detected')

        else:
            rospy.logerr('serial port not open')


    def check_temperature(self, event):
        """
        Check temperature of the housing of the main control board.
        """
        if self.serial_port.isOpen():
            self.lock.acquire()
            # Write request and read answer
            try:
                self.serial_port.write('T\r')
            except:
                rospy.logerr('error sending temperature request')
            try:
                temperature_line = self.serial_port.readline()
            except:
                rospy.logerr('error reading temperature answer')

            self.lock.release()

            # Check answer
            if 'T=' in temperature_line:
                self.diagnostic.add("main_housing_temperature", temperature_line.strip("T=\r"))
        else:
            rospy.logerr('serial port not open')


    def digitalOutputSrv(self, req):
        """ Enables/Disables payload digital outputs """

        rospy.loginfo('Payload digital output %s to %s', req.digital_output, req.value)

        if req.value:
            msg = 'D' + str(req.digital_output) + '1'
        else:
            msg = 'D' + str(req.digital_output) + '0'

        self.lock.acquire()

        # Write request and read answer
        try:
            self.serial_port.write(msg+'\r')
        except serial.SerialException as e:
            rospy.logerr('error sending digital output request: %s', e)
        try:
            d_answer = self.serial_port.readline()
        except serial.SerialException as e:
            rospy.logerr('error reading digital output request answer: %s', e)

        self.lock.release()

        ret = DigitalOutputResponse()
        if '1' in d_answer:
            ret.success = True
        else:
            ret.success = False

        return ret

    def get_config(self):
        """ Get config from param server """

        param_dict = {'serial_path': ('serial_port/path', "/dev/ttyS5"),
                      'serial_baud_rate': ('serial_port/baud_rate', 19200)}

        param_loader.get_ros_params(self, param_dict)


if __name__ == '__main__':
    try:
        rospy.init_node('main_control_board')
        battery_control_board = MainControlBoard(rospy.get_name())
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
