#!/usr/bin/env python

import roslib
import rospy
#roslib.load_manifest('cola2_control')
#roslib.load_manifest('cola2_navigation')

import math
import numpy as np
#from cola2_lib import cola2_ros_lib
from cola2_lib.rosutils import param_loader

from s2_actuators.msg import ThrustersInfo
from cola2_msgs.msg import DVL
from geometry_msgs.msg import Vector3


class SimDvl:
    """ This node is able to simulate a dvl """

    def __init__(self):
        """ Constructor """
        rospy.init_node('sim_dvl')
        self.name = rospy.get_name()
        namespace = rospy.get_namespace()

        # Init vars
        self.surge_velocity = 0.0  # Nil: Maybe need x, y and rot velocity
        self.thrust_force = [0.0, 0.0]
        self.setpoints = [0.0, 0.0]
        self.last_thrusters_callback = rospy.Time.now()
        self.linear_damping = 15.0
        self.quadratic_damping = 40.0
        self.mass = 36.5

        # Config
        self.get_config()
		
        #Init DVL msg
        self.generic_dvl = DVL()
        self.generic_dvl.header.frame_id = namespace + 'dvl'
        self.generic_dvl.velocity = [0.0, 0.0, 0.0]
        self.generic_dvl.velocity_covariance = [0.02, 0.0, 0.0, 0.0, 0.02, 0.0, 0.0, 0.0, 0.02] # Generic covariance (extract from sim_auv_nav_sensors)
        self.generic_dvl.altitude = -1.0                                                        # Not altitud
		
        # Creat publishers
        self.pub_dvl = rospy.Publisher(
            namespace + 'navigator/dvl',
            DVL,
            queue_size = 2)
			
        #Creat Susbscribers
        rospy.Subscriber(
            namespace + 'actuators/thrusters_info', 
            ThrustersInfo,
            self.update_thrusters,
            queue_size = 2)    

        # Show message
        rospy.loginfo(self.name + ': Initialaized')
		
        rospy.spin()

    def update_thrusters(self, data):
        """ Callback of setpoints """
        self.setpoints = data.processed_setpoints

        #Velocity calculation
        self.veloctiy_calculation()

        #Publishing DVL message
        self.publish_dvl()

        self.last_thrusters_callback = rospy.Time.now()

    def veloctiy_calculation(self): 
        """ Algorithm to calculate veloctiy """
        # Compute callback period
        callback_period = (rospy.Time.now() - self.last_thrusters_callback).to_sec()
        if callback_period > 2.0:
            self.setpoints = [0.0, 0.0]
            
        #Compute force
        if self.setpoints[0] > 0.0:
          self.thrust_force[0] = -82.2525498983 * abs(self.setpoints[0])**3 + 156.825663343 * abs(self.setpoints[0])**2 - 3.716104708 * abs(self.setpoints[0])
        elif self.setpoints[0] < 0.0:
          self.thrust_force[0] = -(-62.3125378018 * abs(self.setpoints[0])**3 + 118.8073207144 * abs(self.setpoints[0])**2 -2.8152308394 * abs(self.setpoints[0]))
        else:
          self.thrust_force[0] = 0.0
          
        if self.setpoints[1] > 0.0:
          self.thrust_force[1] = -82.2525498983 * abs(self.setpoints[1])**3 + 156.825663343 * abs(self.setpoints[1])**2 - 3.716104708 * abs(self.setpoints[1])
        elif self.setpoints[1] < 0.0:
          self.thrust_force[1] = -(-62.3125378018 * abs(self.setpoints[1])**3 + 118.8073207144 * abs(self.setpoints[1])**2 -2.8152308394 * abs(self.setpoints[1]))
        else:
          self.thrust_force[1] = 0.0

        #Compute force for each thruster
        #if self.setpoints[0] > 0.1:
        #	self.thrust_force[0] = 9.22619 * self.setpoints[0] * self.setpoints[0] + 73.0059 * self.setpoints[0] - 11.74107
        #elif self.setpoints[0] < -0.1:
        #	self.thrust_force[0] = -20.83333 * abs(self.setpoints[0]) * abs(self.setpoints[0]) - 4.70238 * abs(self.setpoints[0]) + 0.85714
        #else:
        #	self.thrust_force[0] = 0.0

        
        #if self.setpoints[1] > 0.1:
        #	self.thrust_force[1] = 31.84523 * self.setpoints[1] * self.setpoints[1] + 54.4345 * self.setpoints[1] - 6.80655
        #elif self.setpoints[1] < -0.1:
        #	self.thrust_force[1] = -7.44047 * abs(self.setpoints[1]) * abs(self.setpoints[1]) -20.80357 * abs(self.setpoints[1]) + 3.39583
        #else:
        #	self.thrust_force[1] = 0.0	

        # Compute thrust force
        #thrust_force = thrust_setpoint * self.thruster_thrust

        # Compute damping
        if (self.surge_velocity > 0.3): #TODO: Hardcoded (if vel < 0.255 the polinomy has negative value <- This is an error)
        	damping = -(self.linear_damping * abs(self.surge_velocity) + self.quadratic_damping * abs(self.surge_velocity) * abs(self.surge_velocity))
		#elif (self.surge_velocity < -0.255): #TODO: backward damping is not the same as forward
        #	damping = self.linear_damping * abs(self.surge_velocity) + self.quadratic_damping * abs(self.surge_velocity) * abs(self.surge_velocity)
        else:
        	damping = -self.surge_velocity * 20.0 #TODO: Hardcoded

        # Compute total force
        force = self.thrust_force[0] + self.thrust_force[1] + damping

        # Compute acceleration
        acceleration = force / float(self.mass)

        # Compute new velocity
        self.surge_velocity = self.surge_velocity + callback_period * acceleration

        # Print some info
        #rospy.loginfo(self.name + ': thrust_setpoint: ' + thrust_setpoint)
        #rospy.loginfo(self.name + ': thrust_force[0]: %f', self.thrust_force[0])
        #rospy.loginfo(self.name + ': thrust_force[1]: %f', self.thrust_force[1])
        #rospy.loginfo(self.name + ': damping: %f', damping)
        #rospy.loginfo(self.name + ': force: %f', force)
        #rospy.loginfo(self.name + ': acceleration: %f',  acceleration)
        #rospy.loginfo(self.name + ': surge_velocity: %f', self.surge_velocity)

    def publish_dvl(self):
        """ Publish dvl message """
		
        #Translait body velocity in axial velocity
        self.generic_dvl.velocity = Vector3(float(self.surge_velocity), 0.0, 0.0)

        # Publish
        self.generic_dvl.header.stamp = rospy.Time.now()
        self.pub_dvl.publish(self.generic_dvl)             
        #rospy.loginfo(self.name + ': publsishing sim dvl')

    def get_config(self):
        """ Get config from param server """
        param_dict = {'mass': ('mass',
                        70.0),
                    'linear_damping': ('linear_damping',
                        15.0),
                    'quadratic_damping': ('quadratic_damping',
                        40.0)}

        #if not cola2_ros_lib.getRosParams(self, param_dict, self.name):
        if not param_loader.get_ros_params(self, param_dict):
            self.bad_config_timer = rospy.Timer(rospy.Duration(0.4), self.bad_config_message)

        # self.dvl_rotation = PyKDL.Rotation.RPY(self.dvl_rotation[0] * math.pi / 180.0, self.dvl_rotation[1] * math.pi / 180.0, self.dvl_rotation[2] * math.pi / 180.0)


    def bad_config_message(self, event):
        """ Timer to show an error if loading parameters failed """
        rospy.logfatal(self.name + ': bad parameters in param server!')


if __name__ == '__main__':
    try:
        SD = SimDvl()
    except rospy.ROSInterruptException:
        pass

