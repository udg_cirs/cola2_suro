#!/usr/bin/env python

import math
import rospy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PointStamped
from cola2_msgs.msg import NavSts
from cola2_msgs.srv import Goto, GotoRequest 
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
from cola2_msgs.msg import GoalDescriptor

MAX_DISTANCE = 250.0
MIN_DISTANCE = 0.0 #TODO: Why minimum distance was 3.0?

class Intercept(object):
    def __init__(self, name):
        self.name = name
        self.namespace = rospy.get_namespace()
        self.odom = None
        self.goal = None
        self.nav = None
        self.new_data = False

        # Init Service Client
        try:
            rospy.wait_for_service(self.namespace + 'captain/enable_goto', 20)
            self.enable_goto_srv = rospy.ServiceProxy(
                        self.namespace + 'captain/enable_goto', Goto)   
        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error creating client to enable_goto action.',
                         self.name)
            rospy.signal_shutdown('Error creating enable_goto action client')

        try:
            rospy.wait_for_service(self.namespace + 'captain/disable_goto', 20)
            self.disable_goto_srv = rospy.ServiceProxy(
                        self.namespace + 'captain/disable_goto', Empty)
        except rospy.exceptions.ROSException:
            rospy.logerr('%s, Error creating client to disable_goto action.',
                         self.name)
            rospy.signal_shutdown('Error creating disable_goto action client')


        # Subscribers
        self.sub_target_odom = rospy.Subscriber(self.namespace + 'evologics_archrov_suro/target_odometry',
                                                Odometry, self.target_odom_callback)

        self.sub_target_goal = rospy.Subscriber(self.namespace + 'evologics_archrov_suro/target_goal',
                                                PointStamped, self.target_goal_callback)

        self.sub_nav = rospy.Subscriber(self.namespace + 'navigator/navigation',
                                        NavSts, self.nav_callback)

        # Prepare GOTO request
        self.req = GotoRequest()
        self.req.position.z = 0.0
        self.req.altitude = 0.0
        self.req.altitude_mode = False
        self.req.blocking = False
        self.req.keep_position = False 
        self.req.priority = GoalDescriptor.PRIORITY_NORMAL
        self.req.reference = GotoRequest.REFERENCE_NED
        self.req.disable_axis.x = False
        self.req.disable_axis.y = True
        self.req.disable_axis.z = True
        self.req.disable_axis.roll = True
        self.req.disable_axis.pitch = True
        self.req.disable_axis.yaw = False
        self.req.position_tolerance.x = 1.5
        self.req.position_tolerance.y = 1.5
        self.req.position_tolerance.z = 2.0
        self.req.orientation_tolerance.roll = 0.0 
        self.req.orientation_tolerance.pitch = 0.0 
        self.req.orientation_tolerance.yaw = 0.1 
        self.req.linear_velocity.y = 0.0 
        self.req.linear_velocity.z = 0.0 
        self.req.angular_velocity.roll = 0.0 
        self.req.angular_velocity.pitch = 0.0 
        self.req.angular_velocity.yaw = 0.0 


    def target_odom_callback(self, data):
        self.odom = data
        self.new_data = True

    def target_goal_callback(self, data):
        self.goal = data
        self.new_data = True

    def nav_callback(self, data):
        self.nav = data

        # Check if goto hasbeen achieved
        if abs(self.nav.position.north - self.req.position.x) <= MIN_DISTANCE and \
           abs(self.nav.position.east - self.req.position.y) <= MIN_DISTANCE:
               self.new_data = True
               self.req.position.x = self.req.position.x - 10.0
               self.req.position.y = self.req.position.y - 10.0


    def iterate(self):
        if self.new_data:
            # Check that the last goal & odom messages are not too old
            if self.odom != None and rospy.Time.now().to_sec() - self.odom.header.stamp.to_sec() > 30.0:
                self.odom = None
            if self.goal != None and rospy.Time.now().to_sec() - self.goal.header.stamp.to_sec() > 300.0:
                self.goal = None

            # If all the data is available...
            if self.odom != None and self.goal != None and self.nav != None:
                # Compute intercerption point

                # get vehicle mod velocity
                vel = self.odom.twist.twist.linear.x
                if self.odom.twist.twist.linear.x == 0.0:
                    vel = 0.3 #TODO: Why?

                # divide v into vx and vy
                angle = math.atan2(self.goal.point.y - self.odom.pose.pose.position.y,
                                   self.goal.point.x - self.odom.pose.pose.position.x)
                print 'angle: ', angle
                vx = math.cos(angle) * vel
                vy = math.sin(angle) * vel
                print 'vx: ', vx
                print 'vy: ', vy


                t = self.intercept([self.nav.position.north, self.nav.position.east],
                                   [self.odom.pose.pose.position.x, self.odom.pose.pose.position.y, vx, vy],
                                   0.9)
                print 'Goal at ', self.goal.point.x, ',', self.goal.point.y

                if t != None:
                    if t[2] > 7.5: #Reduced from 10.0 to 7.5 - We want more velocity
                        if (self.distance(self.nav.position.north, self.nav.position.east, t[0], t[1]) < self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)):
                            print 'Intercep at ', t[0], ', ', t[1], ' at 1.0m/s. Intersection in ', t[2], ' seconds.'
                            d = self.distance(self.nav.position.north, self.nav.position.east, t[0], t[1])
                            if d < MAX_DISTANCE and d > MIN_DISTANCE:
                                self.req.position.x = t[0]
                                self.req.position.y = t[1]
                                self.req.linear_velocity.x = 1.0 #Hardcoded Velocity
                                self.disable_goto_srv()
                                self.enable_goto_srv(self.req)
                            else:
                                print 'Invalid distance!'
                        else:
                            print 'Intercep to goal at ', self.goal.point.x, ', ',self.goal.point.y, ' at 1.0 m/s'
                            d = self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)
                            if d < MAX_DISTANCE and d > MIN_DISTANCE:
                                self.req.position.x = self.goal.point.x
                                self.req.position.y = self.goal.point.y
                                self.req.linear_velocity.x = 1.0 #Hardcoded velocity
                                self.disable_goto_srv()
                                self.enable_goto_srv(self.req)
                            else:
                                print 'Invalid distance!'

                    else:
                        print 'Intercep to goal at ', self.goal.point.x, ', ',self.goal.point.y, ' at ', vel, 'm/s'
                        d = self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)
                        if d < MAX_DISTANCE and d > MIN_DISTANCE:
                            self.req.position.x = self.goal.point.x
                            self.req.position.y = self.goal.point.y
                            self.req.linear_velocity.x = vel
                            self.disable_goto_srv()
                            self.enable_goto_srv(self.req)
                        else:
                            print 'Invalid distance!'

            # If only target pose is available
            elif self.odom != None:
                print 'Move to target last known position: ', self.odom.pose.pose.position.x, ",", self.odom.pose.pose.position.y
                d = self.distance(self.nav.position.north, self.nav.position.east, self.odom.pose.pose.position.x, self.odom.pose.pose.position.y)
                if d < MAX_DISTANCE and d > MIN_DISTANCE:
                    self.req.position.x = self.odom.pose.pose.position.x
                    self.req.position.y = self.odom.pose.pose.position.y
                    self.req.linear_velocity.x = 1.0
                    self.disable_goto_srv()
                    self.enable_goto_srv(self.req)
                else:
                    print 'Invalid distance!'
            # If only target goal is available (Very rare)
            elif self.goal != None:
                print 'Move to target last known goal: ', self.goal.point.x, ',', self.goal.point.y
                d = self.distance(self.nav.position.north, self.nav.position.east, self.goal.point.x, self.goal.point.y)
                if d < MAX_DISTANCE and d > MIN_DISTANCE:
                    self.req.position.x = self.goal.point.x
                    self.req.position.y = self.goal.point.y
                    self.req.linear_velocity.x = 1.0
                    self.disable_goto_srv()
                    self.enable_goto_srv(self.req)
                else:
                    print 'Invalid distance!'


            self.new_data = False


    def distance(self, current_x, current_y, desired_x, desired_y):
        dist = math.sqrt((desired_x - current_x)**2 + (desired_y - current_y)**2)
        print 'distance to target: ', dist
        return dist

    """
        Return solutions for quadratic
    """
    def quad(self, a, b, c):
        sol = None
        if abs(a) < 0.0:
            if abs(b) < 0.0:
                if abs(c) < 0.0:
                    sol = [0,0]
                else:
                    sol = None
            else:
                sol = [-c/b, -c/b]
        else:
            disc = b*b - 4*a*c
            if disc >= 0:
                disc = math.sqrt(disc)
                a = 2*a
                sol = [(-b-disc)/a, (-b+disc)/a]
            else:
                print 'Error!'
                sol = None

        return sol


    def intercept(self, src, dst, v):
        """
            Chaser can reach a circle x^2 + y^2 = r^2 where r = (V*t)
            Target model is:
            x = x0 + vx*t
            y = y0 + vy*t

            Then,
            * find t in (x0+vx*t)^2 + (y0+vy*t)^2 = (t*v)^2
            * fins x and y from target model
        """

        # Save param in more easy to use vars
        print 'Im at ', src[0], ', ', src[1], ', V:', v
        print 'Underwater Robot at ', dst[0], ', ', dst[1], ' vx: ', dst[2], ' vy: ', dst[3]

        tx = dst[0] - src[0]
        ty = dst[1] - src[1]
        tvx = dst[2]
        tvy = dst[3]

        # Get quadratic equation components
        a = tvx*tvx + tvy*tvy - v*v
        b = 2 * (tvx * tx + tvy * ty)
        c = tx*tx + ty*ty

        # Solve quadratic
        ts = self.quad(a, b, c)

        # Find smallest positive solution
        if ts != None:
            t0 = ts[0]
            t1 = ts[1]
            t = min(t0, t1)
            if (t < 0):
                t = max(t0, t1)
            if (t > 0):
                x = dst[0] + tvx*t
                y = dst[1] + tvy*t
                return [x, y, t]

    def intercept2(self, src, vMag, dst, u):
        Ax = src[0]
        Ay = src[1]
        Bx = dst[0]
        By = dst[1]
        ux = u[0]
        uy = u[1]
        ABx = Bx - Ax
        ABy = By - Ay
        print 'Im at ', src[0], ', ', src[1], ', V:', vMag
        print 'Underwater Robot at ', dst[0], ', ', dst[1], ' vx: ', u[0], ' vy: ', u[1]


        """
            Chaser can reach a circle x^2 + y^2 = r^2 where r = (V*t)
            Target model is:
            x = x0 + vx*t
            y = y0 + vy*t

            Then,
            * find t in (x0+vx*t)^2 + (y0+vy*t)^2 = (t*v)^2
            * fins x and y from target model
        """

        tmp1 = -ABx**2 * uy**2 + ABx**2 * vMag**2 + 2*ABx*ABy*ux*uy - ABy**2 * ux**2 + ABy**2* vMag**2
        tmp2 = -ABx*ux - ABy*uy
        tmp3 = ux**2 + uy**2 - vMag**2

        t1 = (-math.sqrt(tmp1) + tmp2)/tmp3
        t2 = (math.sqrt(tmp1) + tmp2)/tmp3
        if t1 < 0.0:
            t = t2
        elif t2 < 0.0:
            t = t1
        else:
            t = min(t1,t2)

        x_intercept = Bx + ux*t
        y_intercept = By + uy*t

        return [x_intercept, y_intercept, t]


# ==============================================================================
if __name__ == '__main__':

    rospy.init_node('intercept')
    intercept = Intercept(rospy.get_name())
    # Test intercept function
    #print intercept.intercept([20.0, 0.0], [10.0, 5.0, 0.2, -1.0], 1.8)
    #print intercept.intercept2([20.0, 0.0], 1.8, [10.0, 5.0], [0.2, -1.0])

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        intercept.iterate()
        rate.sleep()
